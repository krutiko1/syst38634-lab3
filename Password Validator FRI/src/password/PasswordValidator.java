package password;

/**
 * 
 * Assume spaces are not valid characters for the purpose of calculating length
 * 
 * @author Roman Krutikov 991545938
 *
 */

public class PasswordValidator {

	private final static int MIN_DIGITS = 2;
	private final static int MIN_LENGTH = 8;
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	
	public static boolean isDigitRequirementMet(String password) {
		int numberOfDigits = 0;
		if (password != null) {
			for (char c : password.toCharArray()){
				if (Character.isDigit(c)) {
					numberOfDigits++;
				}
			}
		}
		return numberOfDigits >= MIN_DIGITS;
	}
	
	public static boolean isValidLength(String password) throws NullPointerException{
		if (!password.contains(" ") && password.length() >= MIN_LENGTH) {
			return true;
		}
		return false;
	}
	

	
}
