package password;

/**
 * 
 * @author Roman Krutikov 991545938
 *
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("fdfdRfff"));
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}
	
	
	
	
	
	/*
	
	@Test
	public void testIsDigitRequirementMetRegular() {
		boolean isMet = PasswordValidator.isDigitRequirementMet("Password123");
		assertTrue("Invalid number of digits", isMet);
	}
	
	@Test
	public void testIsDigitRequirementMetException() {
		boolean isMet = PasswordValidator.isDigitRequirementMet(null);
		assertFalse("Invalid number of digits", isMet);
	}
	
	@Test
	public void testIsDigitRequirementMetBoundaryIn() {
		boolean isMet = PasswordValidator.isDigitRequirementMet("Password12");
		assertTrue("Invalid number of digits", isMet);
	}
	
	@Test
	public void testIsDigitRequirementMetBoundaryOut() {
		boolean isMet = PasswordValidator.isDigitRequirementMet("Password1");
		assertFalse("Invalid number of digits", isMet);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	@Test 
	public void testIsValidLengthRegular() {
		boolean passwordLength = PasswordValidator.isValidLength("Password123");
		assertTrue("Invalid Password Length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean passwordLength = PasswordValidator.isValidLength("         ");
		assertFalse("Invalid Password Length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthInBoundary() {
		boolean passwordLength = PasswordValidator.isValidLength("Password");
		assertTrue("Invalid Password Length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthOutBoundary() {
		boolean passwordLength = PasswordValidator.isValidLength("Passwor");
		assertFalse("Invalid Password Length", passwordLength);
	}
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


}
